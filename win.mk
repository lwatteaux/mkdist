# mkdist/win.mk
#
# bdist_wininst

PYTHON_WIN ?= python.exe

REQUIREMENTS_DIR ?= $(SDIST_DIR)/requirements

ifneq ($(TARGET_VERSION),)
target_ver := $(wordlist 1,3,$(subst ., ,$(TARGET_VERSION)))
TARGET_MAJOR := $(word 1,$(target_ver))
TARGET_MINOR := $(word 2,$(target_ver))
bdist_wininst_args += --target-version=$(TARGET_MAJOR).$(TARGET_MINOR)
bdist_msi_args += --target-version=$(TARGET_MAJOR).$(TARGET_MINOR)
ifeq ($(PYWIN_VERSION),)
PYWIN_VERSION := $(TARGET_VERSION)
endif
endif

PYWIN_VERSION ?= 3.7.4
pywin_ver := $(wordlist 1,3,$(subst ., ,$(PYWIN_VERSION)))
PYWIN_MAJOR := $(word 1,$(pywin_ver))
PYWIN_MINOR := $(word 2,$(pywin_ver))

ifeq ($(shell test $(PYWIN_MAJOR)$(PYWIN_MINOR) -gt 34; echo $$?),0)
PYWIN_SETUP_EXT := exe
PY32_SETUP ?= python-$(PYWIN_VERSION)-webinstall.exe
PY64_SETUP ?= python-$(PYWIN_VERSION)-amd64-webinstall.exe
else
PYWIN_SETUP_EXT := msi
PY32_SETUP ?= python-$(PYWIN_VERSION).msi
PY64_SETUP ?= python-$(PYWIN_VERSION).amd64.msi
endif


# Build on Windows 
ifneq ($(OS),Windows_NT)
include mkdist/wine.mk
else
drive_c := c:\\

.PHONY: wine32 wine64
wine32 wine64:
endif

.PHONY: all
all:

.PHONY: bdist_wininfo
bdist_wininfo:
	@echo "TARGET_VERSION    : ${TARGET_VERSION}"
	@echo "TARGET_MAJOR      : ${TARGET_MAJOR}"
	@echo "TARGET_MINOR      : ${TARGET_MINOR}"
	@echo "bdist_wininst_args: $(bdist_wininst_args)"
	@echo "PYWIN             : $(PYWIN_VERSION)"
	@echo "PYWIN ARCH        : $(PYWIN_ARCH)"
	@echo "PYWIN_SETUP       : $(PY$(PYWIN_ARCH)_SETUP)"
	@printf "Installed         : "
	@($(PYTHON_WIN) --version)


$(THIRDPARTY_DIR):
	@(mkdir -pv $@)


# Download Python setup for Windows
.PHONY: pywin32_setup pywin64_setup
pywin32_setup pywin64_setup: pywin%_setup: $(DOWNLOAD_DIR)
	# Download Python setup for Windows
	$(eval PYWIN_ARCH := $*)
	$(eval PYWIN_SETUP := $(PY$(PYWIN_ARCH)_SETUP))
	@if [ ! -f $(DOWNLOAD_DIR)/$(PYWIN_SETUP) ]; then \
		$(MAKE) python_pubkeys && \
		cd $(DOWNLOAD_DIR) && \
		$$(wget -N https://www.python.org/ftp/python/$(PYWIN_VERSION)/$(PYWIN_SETUP) && \
		wget -N https://www.python.org/ftp/python/$(PYWIN_VERSION)/$(PYWIN_SETUP).asc) && \
		gpg --verify $(PYWIN_SETUP).asc; \
	else \
		echo "$(PYWIN_SETUP) already downloaded."; \
	fi


# Install python for windows
.PHONY: pywin32 pywin64
pywin32 pywin64: pywin%: wine% pywin%_setup
	# Install Python for Windows
	$(eval TARGET_DIR := Python$(PYWIN_MAJOR)$(PYWIN_MINOR)-$(PYWIN_ARCH))
	@if [ ! -d $(drive_c)/$(TARGET_DIR) ] && [ -f $(DOWNLOAD_DIR)/$(PYWIN_SETUP) ]; then \
		if [ $(PYWIN_SETUP_EXT) = msi ]; then \
		msiexec /i "$(DOWNLOAD_DIR)/$(PYWIN_SETUP)" /passive TARGETDIR="C:\\$(TARGET_DIR)" ALLUSERS=1 ADDLOCAL=ALL REMOVE="Documentation,Testsuite" && \
			if [ $(PYWIN_MAJOR)$(PYWIN_MINOR) = 34 ] && [ $(WINEARCH) = win64 ]; then \
				cd $(drive_c)/$(TARGET_DIR)/Lib/distutils/command && \
				ln -s wininst-10.0-amd64.exe wininst-7.1-amd64.exe; \
			fi; \
		else \
			test -d $(DOWNLOAD_DIR)/$(TARGET_DIR)_msi || mkdir $(DOWNLOAD_DIR)/$(TARGET_DIR)_msi && \
			cd $(DOWNLOAD_DIR)/$(TARGET_DIR)_msi && \
			$(WINE) "$(DOWNLOAD_DIR)/$(PYWIN_SETUP)" /layout . /quiet && \
			for msi in core dev exe launcher lib path pip tcltk tools ucrt; do \
				msiexec /i $$msi.msi targetdir="C:\\$(TARGET_DIR)"; \
			done; \
		fi; \
	else \
		echo "$(PYWIN_SETUP) already installed."; \
	fi
	# End install Python for windows


# Get required packages
.PHONY: requirements_win32 requirements_win_amd64
requirements_win32 requirements_win_amd64: requirements_%: $(THIRDPARTY_DIR)
	# Get required packages
	if [ -f $(REQUIREMENTS_DIR)/$*.txt ] && [ $(TARGET_VERSION) ] && [ -d $(THIRDPARTY_DIR) ]; then \
		if [ $(TARGET_MAJOR) = $(PYWIN_MAJOR) ] && [ $(TARGET_MINOR) = $(PYWIN_MINOR) ]; then \
			$(PYTHON_WIN) -m pip download -d $(THIRDPARTY_DIR) -r $(REQUIREMENTS_DIR)/$*.txt; \
		else \
			$(PYTHON_WIN) -m pip download -d $(THIRDPARTY_DIR) -r $(REQUIREMENTS_DIR)/$*.txt --python-version $(TARGET_MAJOR)$(TARGET_MINOR) --only-binary=:all:; \
		fi; \
	fi

requirements_any: requirements_%:
	$(PYTHON) -m pip download --platform $* --python-version $(TARGET_MAJOR)$(TARGET_MINOR) --only-binary=:all: -d $(THIRDPARTY_DIR) -r $(REQUIREMENTS_DIR)/$*.txt; \


# Get and install python environment
.PHONY: mingw_setup mingw
mingw_setup: $(DOWNLOAD_DIR)
	@(cd $(DOWNLOAD_DIR) && test -f mingw-get-setup.exe || \
		wget -N https://sourceforge.net/projects/mingw/files/Installer/mingw-get-setup.exe)

mingw: mingw_setup
	@(test -d $(drive_c)/MinGW || \
		$(WINE) $(DOWNLOAD_DIR)/mingw-get-setup.exe)

bdist_msi_help: sdist_tmp
	@cd $(SDIST_DIR) && $(PYTHON_WIN) $(SETUP_PY) bdist_msi --help


.PHONY: bdist_msi
bdist_msi: sdist_tmp $(DIST_DIR)
	@echo "bdist_wininst_args: $(bdist_wininst_args)"
	## Windows installer .msi
	cd $(SDIST_DIR) && $(PYTHON_WIN) $(SETUP_PY) $@ $($@_args) -d $(DIST_DIR)
	$(MAKE) clean_sdist

.PHONY: bdist_msi32 bdist_msi64
bdist_msi32: pywin32 sdist_tmp thirdparty_win32 bdist_msi
bdist_msi64: pywin64 sdist_tmp thirdparty_win_amd64 bdist_msi


.PHONY: bdist_wininst
bdist_wininst: sdist_tmp $(DIST_DIR)
	## Windows installer .exe
	cd $(SDIST_DIR) && $(PYTHON_WIN) $(SETUP_PY) $@ $($@_args) -d $(DIST_DIR)
	$(MAKE) clean_sdist

.PHONY: bdist_win32 bdist_win64
bdist_win32: pywin32 sdist_tmp requirements_win32 bdist_wininst
bdist_win64: pywin64 sdist_tmp requirements_win_amd64 bdist_wininst


.PHONY: bdist_win32_noinstallpy bdist_win64_noinstallpy
bdist_win32_noinstallpy: sdist_tmp requirements_win32 bdist_wininst
bdist_win64_noinstallpy: sdist_tmp requirements_win_amd64 bdist_wininst


# Run builded setup
.PHONY: run_setup_win32 run_setup_win-amd64
run_setup_win32 run_setup_win-amd64: run_setup_%:
	@if [ $(TARGET_VERSION) ]; then \
		$(WINE) $(DIST_DIR)/$(FULLNAME).$*-py$(TARGET_MAJOR).$(TARGET_MINOR).exe; \
	else \
		$(WINE) $(DIST_DIR)/$(FULLNAME).$*.exe; \
	fi
