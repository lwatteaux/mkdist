# mkdist/deb.mk 
#
# Debian/Ubuntu/Mint .deb

# bdist_deb_args ?=

.PHONY: all
all:

.PHONY: bdist_deb
bdist_deb: sdist_tmp $(DIST_DIR)
	@(cd $(SDIST_DIR) && $(PYTHON) $(SETUP_PY) --command-packages=stdeb.command $@ $($@_args) -d $(DIST_DIR))
	$(MAKE) clean_sdist
