# mkdist/release.mk
#
# release sdist, wheels, dist/*, ... on pypi

RELEASE_EXT ?= *$(VERSION)*.tar.gz

REPOSITORY_URL ?= https://upload.pypi.org/legacy/
REPOSITORY_TEST_URL ?= https://test.pypi.org/legacy/


.PHONY: all
all:

.PHONY: release_files release_infos release_pypi_test release_pypi

release_files:
	$(eval RELEASE_FILES := $(DIST_DIR)/$(RELEASE_EXT))

release_infos: distinfos release_files
	@(echo "RELEASE_FILES       : $(RELEASE_FILES)")
	@(echo "REPOSITORY_URL      : $(REPOSITORY_URL)")
	@(echo "REPOSITORY_TEST_URL : $(REPOSITORY_TEST_URL)")


release_pypi_test: release_files
	$(PYTHON) -m twine upload --verbose --repository-url $(REPOSITORY_TEST_URL) $(RELEASE_FILES)

release_pypi: release_files
	$(PYTHON) -m twine upload --verbose --repository-url $(REPOSITORY_URL) $(RELEASE_FILES)
