# mkdist/wine.mk
#
# GNU/Linux: Install 'winbind' distribution package could avoid errors
# bdist_wininst out of windows

WINE ?= wine

PYTHON_WIN := $(WINE) $(PYTHON_WIN)

WINEPREFIX := $(BUILD_DIR)/.wine
export WINEPREFIX

WINEARCH ?= win64
export WINEARCH

drive_c := $(WINEPREFIX)/drive_c

.PHONY: all
all:

.PHONY: wineinfos
wineinfos:
	@echo "WINEPREFIX        : $(WINEPREFIX)"
	@echo "WINEARCH          : $(WINEARCH)"
	@printf "WINE              : "
	@$(WINE) --version

.PHONY: winearch32 winearch64
winearch32 winearch64: winearch%:
	$(eval WINEARCH := win$*)

clean_wine:
	@if [ -d $(WINEPREFIX) ]; then \
		echo "Delete WINEPREFIX: $(WINEPREFIX) ?"; \
		rm -rfI $(WINEPREFIX); \
	fi

.PHONY: wine32 wine64 clean_wine
wine32 wine64: wine%: $(BUILD_DIR) clean_wine winearch% wineinfos
	wineboot -i


.PHONY: regedit
regedit: wineinfos
	regedit

.PHONY: winecfg
winecfg: wineinfos
	winecfg

.PHONY: winecmd
winecmd: wineinfos
	$(WINE) cmd
