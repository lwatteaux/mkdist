mkdist
======

mkdist is a suite of makefiles to simplify python packaging


Distributions
-------------

    * sdist (tar.gz) - Python
    * wheel (.whl)   - Python
    * win32 (.exe)   - Windows
    * win64 (.exe)   - Windows
    * rpm   (.rpm)   - Red Hat (Fedora, Centos)
    * deb   (.deb)   - Debian/Ubuntu/Mint
     

Installation
------------

:: 

    cd you/python/project
 
    git clone https://gitlab.com/lwatteaux/mkdist.git
 
    cp mkdist/Makefile.example Makefile


edit Makefile to ajust distribution arguments(wininst, rpm, deb, wheel), ...

Only include dist.mk is needed in your Makefile:
::

    include mkdist/dist.mk



Usage
-----
::

    make target_xxxx


List of targets
---------------
    * dist
    * sdist
    * bdist_win32
    * bdist_win64
    * bdist_wininst
    * bdist_wheel
    * bdist_rpm
    * bdist_deb

Authors
-------
Ludovic Watteaux


License
-------

mkdist is licenced under GNU GENERAL PUBLIC LICENSE Version 3

For full license text: http://www.gnu.org/copyleft/gpl.html
