# mkdist/dist.mk 
#
# author: Ludovic Watteaux
#
# dist makefile utils

PYTHON ?= python

unexport PYTHONSTARTUP

PYTHON_VERSION := $(shell $(PYTHON) -c "import sys; sys.stdout.write('%d.%d' % sys.version_info[0:2])")

OS ?= $(shell sh -c 'uname 2>/dev/null || echo Unknown')

SETUP_PY ?= setup.py

NAME := $(shell $(PYTHON) $(SETUP_PY) --name)
VERSION := $(shell $(PYTHON) $(SETUP_PY) --version)
FULLNAME := $(shell $(PYTHON) $(SETUP_PY) --fullname)
SDIST_DIR := $(FULLNAME)
DIST_DIR ?= ${CURDIR}/dist
BUILD_DIR := ${CURDIR}/build
DOWNLOAD_DIR ?= ${CURDIR}/build/.download
THIRDPARTY_DIR ?= $(DONLOAD_DIR)

GNUPGHOME := ${BUILD_DIR}/.gnupg
export GNUPGHOME

AUTO_CLEAN ?= 1

#########
# bdist #
#########
# python wheel
# bdist_wheel_args ?= --build-number=

# windows
include mkdist/win.mk
 
# Red Hat (centos/fedora) .rpm
include mkdist/rpm.mk

# Debian/Ubuntu/Mint .deb
include mkdist/deb.mk

# release
include mkdist/release.mk


.PHONY: all
all:

.PHONY: distinfos sdist sdist_tmp bdist_wheel clean

$(BUILD_DIR) $(DOWNLOAD_DIR) $(GNUPGHOME) $(DIST_DIR):
	@(mkdir -pv $@)

distinfos:
	@(echo "Name          : $(NAME)")
	@(echo "Version       : $(VERSION)")
	@(echo "Fullname      : ${FULLNAME}")
	@(echo "OS            : $(OS)")
	@(echo "PYTHON        : ${PYTHON_VERSION}")
	@(echo "CURDIR        : ${CURDIR}")
	@(echo "DIST_DIR      : ${DIST_DIR}")
	@(echo "DOWNLOAD_DIR  : ${DOWNLOAD_DIR}")
	@(echo "THIRDPARTY_DIR: ${THIRDPARTY_DIR}")
	@(echo "bdist_wheel_args  : "$(bdist_wheel_args))
	@(echo "bdist_deb_args    : "$(bdist_deb_args))
	@(echo "bdist_rpm_args    : "$(bdist_rpm_args))
	@(echo "bdist_msi_args    : "$(bdist_msi_args))
	@(echo "bdist_wininst_args: "$(bdist_wininst_args))


clean: clean_sdist
	@($(PYTHON) $(SETUP_PY) clean --all)

sdist: clean_sdist
	@($(PYTHON) $(SETUP_PY) $@ $($@_args) -d $(DIST_DIR))

sdist_tmp: clean_sdist
	@($(PYTHON) $(SETUP_PY) sdist --keep-temp -d $(DIST_DIR) $(sdist_args))

# Python wheel .whl
bdist_wheel: sdist_tmp $(DIST_DIR)
	@(cd $(SDIST_DIR) && $(PYTHON) $(SETUP_PY) $@ $($@_args) -d $(DIST_DIR))
	$(MAKE) clean_sdist

# Current os bytes compiled python tar.gz package
bdist: sdist_tmp
	@(cd $(SDIST_DIR) && $(PYTHON) $(SETUP_PY) $@ $($@_args) -d $(DIST_DIR))

# Cleaning
.PHONY: clean_sdist
clean_sdist: clean_SDIST_DIR

.PHONY: clean_SDIST_DIR clean_BUILD_DIR clean_GNUPGHOME clean_DOWNLOAD
clean_SDIST_DIR clean_BUILD_DIR clean_GNUPGHOME clean_DOWNLOAD: clean_%:
	@if [ -d $($*) ] ; then \
		if [ $(AUTO_CLEAN) = 1 ]; then \
			echo "deleting $($*)"; \
			rm -rf $($*); \
		else \
			echo "delete $($*) ?"; \
			rm -rfI $($*); \
		fi; \
	fi


.PHONY: python_pubkeys
python_pubkeys: clean_GNUPGHOME $(GNUPGHOME)
	@cd $(DOWNLOAD_DIR) && \
	wget -N https://www.python.org/static/files/pubkeys.txt && \
	gpg --import pubkeys.txt


.PHONY: gpginfos
gpginfos: $(GNUPGHOME)
	gpg --list-keys
