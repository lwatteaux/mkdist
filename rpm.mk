# mkdist/rpm.mk 
#
# Red Hat (centos/fedora) .rpm

bdist_rpm_args ?= --source-only

.PHONY: all
all:

.PHONY: bdist_rpm
bdist_rpm: sdist_tmp $(DIST_DIR)
	@(cd $(SDIST_DIR) && $(PYTHON) $(SETUP_PY) $@ $($@_args) -d $(DIST_DIR))
	$(MAKE) clean_sdist
